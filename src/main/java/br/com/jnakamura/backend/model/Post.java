package br.com.jnakamura.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "post-1")
    private String text;

    @NotNull(message =  "post-2")
    @DecimalMin(value="0", message = "post-3")
    private Long likes;

    @JsonIgnore
    public void addLikes() {
        this.likes += 1L;
    }
}
