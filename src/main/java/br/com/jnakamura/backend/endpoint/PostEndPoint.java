package br.com.jnakamura.backend.endpoint;

import br.com.jnakamura.backend.model.Post;
import br.com.jnakamura.backend.repository.PostRepository;
import br.com.jnakamura.backend.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostEndPoint {

    @Autowired
    PostRepository postRepository;

    @Autowired
    PostService postService;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Post> getAll() {
        return postRepository.findAll(new Sort(Sort.Direction.ASC, "id")) ;
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Post newPost(@Valid @RequestBody Post postModel) {

        return postService.save(postModel);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("{id}/like/add")
    @ResponseStatus(HttpStatus.OK)
    public Post addLikes(@PathVariable Long id)  {
        return postService.addLikes(id);
    }




}
