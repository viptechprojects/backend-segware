package br.com.jnakamura.backend.service.exception;

import org.springframework.http.HttpStatus;

public class PostNotHasFiveDigitsException extends BusinessException {


    public PostNotHasFiveDigitsException() {
        super("post-4", HttpStatus.BAD_REQUEST);
    }
}
