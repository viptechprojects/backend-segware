package br.com.jnakamura.backend.service.validation;

import br.com.jnakamura.backend.model.Post;
import br.com.jnakamura.backend.repository.PostRepository;

public interface BusinessValidation {

    void apply(Post beerModel, PostRepository repository);

}
