package br.com.jnakamura.backend.service.validation;

import br.com.jnakamura.backend.model.Post;
import br.com.jnakamura.backend.repository.PostRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class BusinessValidator implements BusinessValidatorHandle{

    private final List<BusinessValidation> validations;
    private final Post postModel;
    private PostRepository postRepository;

    @Override
    public void applyValidations() {
        this.validations.forEach(validation -> {
            validation.apply(postModel, postRepository);
        });
    }


}

