package br.com.jnakamura.backend.service.validation;

import br.com.jnakamura.backend.model.Post;
import br.com.jnakamura.backend.repository.PostRepository;
import br.com.jnakamura.backend.service.exception.PostNotFoundException;

import java.util.Optional;

public class PostNotExistValidation implements BusinessValidation {
    @Override
    public void apply(Post postModel, PostRepository repository) {

        if(postModel == null) {
            throw new PostNotFoundException();
        }
        Optional<Post> postRecovery = repository.findById(postModel.getId());


        if(!postRecovery.isPresent()) {
        }

    }
}
