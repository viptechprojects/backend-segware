package br.com.jnakamura.backend.service.exception;

import org.springframework.http.HttpStatus;

public class PostNotFoundException extends BusinessException {

    public PostNotFoundException() {
        super("post-5", HttpStatus.NOT_FOUND);
    }
}
