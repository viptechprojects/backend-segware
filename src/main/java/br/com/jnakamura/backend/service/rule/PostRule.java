package br.com.jnakamura.backend.service.rule;

import br.com.jnakamura.backend.service.validation.BusinessValidation;

import java.util.List;

public interface PostRule {

    List<BusinessValidation> get();

}
