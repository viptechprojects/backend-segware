package br.com.jnakamura.backend.service.validation;

import br.com.jnakamura.backend.model.Post;
import br.com.jnakamura.backend.repository.PostRepository;
import br.com.jnakamura.backend.service.exception.PostNotHasFiveDigitsException;

public class PostTextMinValueOfDigitsValidation implements BusinessValidation {

    @Override
    public void apply(Post postModel, PostRepository postRepository) {

        String text = postModel.getText().trim();

        if(text.isEmpty() || text.length() < 5) {
            throw new PostNotHasFiveDigitsException();
        }
    }
}
