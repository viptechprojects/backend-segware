package br.com.jnakamura.backend.service;

import br.com.jnakamura.backend.model.Post;
import br.com.jnakamura.backend.service.exception.PostNotFoundException;
import br.com.jnakamura.backend.service.rule.CreatePostRule;
import br.com.jnakamura.backend.service.validation.BusinessValidator;
import br.com.jnakamura.backend.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;

    public Post save(Post postModel) {
        BusinessValidator validator = new BusinessValidator(new CreatePostRule().get(), postModel, postRepository);
        validator.applyValidations();
        return postRepository.save(postModel);
    }

    public Post addLikes(Long id) {
        Optional<Post> postRecovery = postRepository.findById(id);
        if(!postRecovery.isPresent()) {
            throw new PostNotFoundException();
        }
        Post postModel = postRecovery.get();
        postModel.addLikes();
        return postRepository.save(postModel);
    }


}
