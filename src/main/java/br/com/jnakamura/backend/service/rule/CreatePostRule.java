package br.com.jnakamura.backend.service.rule;

import br.com.jnakamura.backend.service.validation.BusinessValidation;
import br.com.jnakamura.backend.service.validation.PostTextMinValueOfDigitsValidation;

import java.util.Arrays;
import java.util.List;

public class CreatePostRule implements PostRule {

    @Override
    public List<BusinessValidation> get() {
        return Arrays.asList(new PostTextMinValueOfDigitsValidation());
    }
}
