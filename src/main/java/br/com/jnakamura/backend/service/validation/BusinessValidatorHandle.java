package br.com.jnakamura.backend.service.validation;

import java.util.List;

public interface BusinessValidatorHandle{

      void applyValidations();

}
